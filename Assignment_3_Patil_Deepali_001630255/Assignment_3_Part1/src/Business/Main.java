/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author DeepaliP
 */
public class Main {

    public static void main(String[] args) {

        SuppliersList s = new SuppliersList();
        Suppliers supplier1 = s.addSuppliers();
        supplier1.setSupplier("Dell");

        Suppliers supplier2 = s.addSuppliers();
        supplier2.setSupplier("HP");

        Suppliers supplier3 = s.addSuppliers();
        supplier3.setSupplier("Toshiba");

        Suppliers supplier4 = s.addSuppliers();
        supplier4.setSupplier("Apple");

        Suppliers supplier5 = s.addSuppliers();
        supplier5.setSupplier("Lenovo");

        Products product1 = supplier1.getProductList().addProducts();
        product1.setProduct("Dell_1");

        Products product2 = supplier1.getProductList().addProducts();
        product2.setProduct("Dell_2");

        Products product3 = supplier1.getProductList().addProducts();
        product3.setProduct("Dell_3");

        Products product4 = supplier1.getProductList().addProducts();
        product4.setProduct("Dell_4");

        Products product5 = supplier1.getProductList().addProducts();
        product5.setProduct("Dell_5");

        Products product6 = supplier2.getProductList().addProducts();
        product6.setProduct("HP_1");

        Products product7 = supplier2.getProductList().addProducts();
        product7.setProduct("HP_2");

        Products product8 = supplier2.getProductList().addProducts();
        product8.setProduct("HP_3");

        Products product9 = supplier2.getProductList().addProducts();
        product9.setProduct("HP_4");

        Products product10 = supplier2.getProductList().addProducts();
        product10.setProduct("HP_5");

        Products product11 = supplier3.getProductList().addProducts();
        product11.setProduct("Toshiba_1");

        Products product12 = supplier3.getProductList().addProducts();
        product12.setProduct("Toshiba_2");

        Products product13 = supplier3.getProductList().addProducts();
        product13.setProduct("Toshiba_3");

        Products product14 = supplier3.getProductList().addProducts();
        product14.setProduct("Toshiba_4");

        Products product15 = supplier3.getProductList().addProducts();
        product15.setProduct("Toshiba_5");

        Products product16 = supplier4.getProductList().addProducts();
        product16.setProduct("Apple_1");

        Products product17 = supplier4.getProductList().addProducts();
        product17.setProduct("Apple_2");

        Products product18 = supplier4.getProductList().addProducts();
        product18.setProduct("Apple_3");

        Products product19 = supplier4.getProductList().addProducts();
        product19.setProduct("Apple_4");

        Products product20 = supplier4.getProductList().addProducts();
        product20.setProduct("Apple_5");

        Products product21 = supplier5.getProductList().addProducts();
        product21.setProduct("Lenovo_1");

        Products product22 = supplier5.getProductList().addProducts();
        product22.setProduct("Lenovo_2");

        Products product23 = supplier5.getProductList().addProducts();
        product23.setProduct("Lenovo_3");

        Products product24 = supplier5.getProductList().addProducts();
        product24.setProduct("Lenovo_4");

        Products product25 = supplier5.getProductList().addProducts();
        product25.setProduct("Lenovo_5");

        for (Suppliers supplier : s.getSuppliersList()) {
            System.out.println("******" + supplier.getSupplier() + "******");

            for (Products products : supplier.productList.getProductList()) {

                System.out.println(products.getProduct());

            }

            System.out.println("---------------------");

        }

    }

}
