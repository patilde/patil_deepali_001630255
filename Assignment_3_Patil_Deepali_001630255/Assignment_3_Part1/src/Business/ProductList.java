/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class ProductList {
    
    
    
    private ArrayList<Products> productList;
    
    public ProductList(){
        productList = new ArrayList<>();
    }

    public ArrayList<Products> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Products> productList) {
        this.productList = productList;
    }
    
    public Products addProducts(){
        Products product = new Products();
        productList.add(product);
        return product;
    }
    
}
