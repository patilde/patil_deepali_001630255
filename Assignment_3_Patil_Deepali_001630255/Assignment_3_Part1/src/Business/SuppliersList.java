/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class SuppliersList {
    
    private ArrayList<Suppliers> suppliersList;
    
    public SuppliersList(){
        suppliersList = new ArrayList<>();
    }

    public ArrayList<Suppliers> getSuppliersList() {
        return suppliersList;
    }

    public void setSuppliersList(ArrayList<Suppliers> suppliersList) {
        this.suppliersList = suppliersList;
    }

     public Suppliers addSuppliers(){  
      Suppliers suppliers = new Suppliers();
        suppliersList.add(suppliers);
        return suppliers;
    }
    
}
