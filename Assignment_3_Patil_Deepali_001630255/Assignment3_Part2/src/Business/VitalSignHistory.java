/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class VitalSignHistory {
    
    private ArrayList<VitalSign> vitalSignArrayList;
    
    public VitalSignHistory()
    {
        vitalSignArrayList = new ArrayList<>();  
        
    }

    public ArrayList<VitalSign> getVitalSignArrayList() {
        return vitalSignArrayList;
    }

    public void setVitalSignArrayList(ArrayList<VitalSign> vitalSignArrayList) {
        this.vitalSignArrayList = vitalSignArrayList;
    }
    
    public VitalSign addElements(){
    VitalSign vitalSign = new VitalSign();
    vitalSignArrayList.add(vitalSign); 
    return vitalSign;
    }
    
    
    
}
