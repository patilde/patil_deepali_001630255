/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class PersonHistory {

    private ArrayList<Person> personHistory;

    public PersonHistory() {
        personHistory = new ArrayList<>();

    }

    public ArrayList<Person> getPersonHistory() {
        return personHistory;
    }

    public void setPersonHistory(ArrayList<Person> personHistory) {
        this.personHistory = personHistory;
    }

    public void deleteAccount(Person person) {
        personHistory.remove(person);
    }

    public Person addElements() {
        Person person = new Person();
        personHistory.add(person);
        return person;
    }

    public Person searchPersonById(String id) {
        for (Person person : personHistory) {

            if (person.getId().equals(id)) {
                System.out.println("yes");
                return person;
            }

        }
        System.out.println("No");

        return null;
    }
    
      public Person searchPatientId(String ssn) {
        for (Person person : personHistory) {

            if (ssn.equals(String.valueOf(person.getPatient().getPatientId()))) {
                System.out.println("yes");
                return person;
            }

        }
        System.out.println("No");

        return null;
    }

}
