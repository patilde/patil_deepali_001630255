/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class PatientInformation {

    private String patientName;
    private int patientId;
    private int age;
    private String doctorName;
    private String pharmacyName;
    private VitalSignHistory vitalSigns;

    public PatientInformation() {
        this.vitalSigns = new VitalSignHistory();
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }

    public VitalSignHistory getVitalSigns() {
        return vitalSigns;
    }

    public void setVitalSigns(VitalSignHistory vitalSigns) {
        this.vitalSigns = vitalSigns;
    }
    
    @Override
    public String toString() {
        return doctorName;
    }

}
