/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.PatientInformation;
import Business.Person;
import Business.PersonHistory;
import Business.VitalSign;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author DeepaliP
 */
public class ViewInformationJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewInformation
     */
    private PersonHistory personHistory;
    private JPanel userProcessContainer;

    public ViewInformationJPanel(JPanel userProcessContainer, PersonHistory personHistory) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.personHistory = personHistory;
        populateTable();
        //refreshTable();

    }
       public void refreshTable() {
     int selectedRow1 = tblPerson.getSelectedRow();
     Person person1 = (Person) tblPerson.getValueAt(selectedRow1, 0);
            PatientInformation patientInformation1 = person1.getPatient();
            
        DefaultTableModel dtm3 = (DefaultTableModel) tblSigns.getModel();
        dtm3.setRowCount(0);
        
         for(VitalSign vitalSign : patientInformation1.getVitalSigns().getVitalSignArrayList() )
            {
            
            Object row2[] = new Object[5];
            row2[0] = vitalSign;
             row2[1] = vitalSign.getHeartRate();
              row2[2] = vitalSign.getSystolicBloodPressure();
               row2[3] = vitalSign.getWeight();

            if (1 <= patientInformation1.getAge() && patientInformation1.getAge() <= 3) {
                
                if (20 <= vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 30
                        & 80 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 130
                        & 80 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 110
                        & 22 <= vitalSign.getWeight() && vitalSign.getWeight() <= 31) {
                    
                    row2[4] = "NORMAL";

                } else {
                    System.out.println("Bye");
                    row2[4] = "ABNORMAL";
                }
            }

            if (4 <= patientInformation1.getAge() && patientInformation1.getAge() <= 5) {
                if (20 < vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 20
                        & 80 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 120
                        & 80 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 110
                        & 31 <= vitalSign.getWeight() && vitalSign.getWeight() <= 40) {
                    row2[4] = "NORMAL";

                } else {
                    row2[4] = "ABNORMAL";
                }
            }

            if (6 <= patientInformation1.getAge() && patientInformation1.getAge() <= 12) {
                if (20 < vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 30
                        & 70 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 110
                        & 80 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 120
                        & 41 <= vitalSign.getWeight() && vitalSign.getWeight() <= 92) {
                    row2[4] = "NORMAL";

                } else {
                    row2[4] = "ABNORMAL";
                }
            }

            if (13 <= patientInformation1.getAge()) {
                if (12 < vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 20
                        & 55 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 105
                        & 110 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 120
                        & 110 < vitalSign.getWeight()) {
                    row2[4] = "NORMAL";

                } else {
                    row2[4] = "ABNORMAL";
                }
            } 
               
               
               
               
               dtm3.addRow(row2);
            }

        

    }

    private void populateTable() {

        DefaultTableModel dtm = (DefaultTableModel) tblPerson.getModel();
        int rowCount = dtm.getRowCount();

        for (int x = rowCount - 1; x >= 0; x--) {
            dtm.removeRow(x);
        }

        for (Person person : personHistory.getPersonHistory()) {

            Object row[] = new Object[5];
            row[0] = person;
            row[1] = person.getLastName();
            row[2] = person.getStreetAddress();
            row[3] = person.getId();
            row[4] = person.getPhoneNumber();
            dtm.addRow(row);

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblView = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPerson = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSigns = new javax.swing.JTable();

        setBackground(new java.awt.Color(51, 255, 204));

        lblView.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        lblView.setText("View Panel");

        jButton1.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        jButton1.setText("<<back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        tblPerson.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "First Name", "Last Name", "Address", "ID", "Phone No"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblPerson.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblPersonMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblPerson);

        tblSigns.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Respiratory Rate", "Heart Rate", "BP", "Weight", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tblSigns);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(453, 453, 453)
                        .addComponent(lblView))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(201, 201, 201)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 551, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 583, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(214, 214, 214)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(124, 124, 124)
                        .addComponent(btnUpdate)))
                .addContainerGap(600, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblView)
                .addGap(86, 86, 86)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdate))
                .addContainerGap(433, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed

        int selectedRow = tblSigns.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row first");
        } else {
            VitalSign vitalSign = (VitalSign) tblSigns.getValueAt(selectedRow, 0);
            UpdateVitalSIgnJPanel updateVitalSIgn = new UpdateVitalSIgnJPanel(userProcessContainer, vitalSign);
            userProcessContainer.add("updateVitalSIgn", updateVitalSIgn);
            CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
            cardLayout.next(userProcessContainer);
        }
        btnUpdate.setEnabled(false);
       

    }//GEN-LAST:event_btnUpdateActionPerformed

    private void tblPersonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPersonMousePressed
        // TODO add your handling code here:
        System.out.println("hello");
        int selectedRow = tblPerson.getSelectedRow();

        if (selectedRow >= 0) {
            Person person = (Person) tblPerson.getValueAt(selectedRow, 0);
            PatientInformation patientInformation = person.getPatient();

            DefaultTableModel dtm6 = (DefaultTableModel) tblSigns.getModel();
            int rowCount = dtm6.getRowCount();

            for (int x = rowCount - 1; x >= 0; x--) {
                dtm6.removeRow(x);
            }

            for (VitalSign vitalSign : patientInformation.getVitalSigns().getVitalSignArrayList()) {

                Object row2[] = new Object[5];
                row2[0] = vitalSign;
                row2[1] = vitalSign.getHeartRate();
                row2[2] = vitalSign.getSystolicBloodPressure();
                row2[3] = vitalSign.getWeight();

                if (1 <= patientInformation.getAge() && patientInformation.getAge() <= 3) {
                    System.out.println("HIIII");
                    if (20 <= vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 30
                            & 80 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 130
                            & 80 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 110
                            & 22 <= vitalSign.getWeight() && vitalSign.getWeight() <= 31) {
                        System.out.println("Hello");
                        row2[4] = "NORMAL";

                    } else {
                        System.out.println("Bye");
                        row2[4] = "ABNORMAL";
                    }
                }

                if (4 <= patientInformation.getAge() && patientInformation.getAge() <= 5) {
                    if (20 < vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 20
                            & 80 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 120
                            & 80 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 110
                            & 31 <= vitalSign.getWeight() && vitalSign.getWeight() <= 40) {
                        row2[4] = "NORMAL";

                    } else {
                        row2[4] = "ABNORMAL";
                    }
                }

                if (6 <= patientInformation.getAge() && patientInformation.getAge() <= 12) {
                    if (20 < vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 30
                            & 70 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 110
                            & 80 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 120
                            & 41 <= vitalSign.getWeight() && vitalSign.getWeight() <= 92) {
                        row2[4] = "NORMAL";

                    } else {
                        row2[4] = "ABNORMAL";
                    }
                }

                if (13 <= patientInformation.getAge()) {
                    if (12 < vitalSign.getRespiratoryRate() && vitalSign.getRespiratoryRate() <= 20
                            & 55 <= vitalSign.getHeartRate() && vitalSign.getHeartRate() <= 105
                            & 110 <= vitalSign.getSystolicBloodPressure() && vitalSign.getSystolicBloodPressure() <= 120
                            & 110 < vitalSign.getWeight()) {
                        row2[4] = "NORMAL";

                    } else {
                        row2[4] = "ABNORMAL";
                    }
                }

                dtm6.addRow(row2);
            }
        }

    }//GEN-LAST:event_tblPersonMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblView;
    private javax.swing.JTable tblPerson;
    private javax.swing.JTable tblSigns;
    // End of variables declaration//GEN-END:variables
}
