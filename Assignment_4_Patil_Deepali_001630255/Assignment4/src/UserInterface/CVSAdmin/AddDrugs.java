package UserInterface.CVSAdmin;

import Business.CvsAdmin;
import Business.Drugs;
import Business.DrugsCatalog;
import Business.ManuDrugs;
import Business.Manufacturer;
import Business.ManufacturerList;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class AddDrugs extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private CvsAdmin cvsAdmin;
    private ManageDrugs manageDrugs;
    private ManufacturerList manufacturerList;
    private Manufacturer manufacturer6;
    private ManuDrugs manuDrugss;
    private Drugs drugs;

    public AddDrugs(JPanel upc, CvsAdmin cvsAdmin, ManageDrugs manageDrugs, ManufacturerList manufacturerList) {
        initComponents();
        System.out.println("AddDrugs Constructor");
        this.userProcessContainer = upc;
        this.cvsAdmin = cvsAdmin;
        this.manageDrugs = manageDrugs;
        this.manufacturerList = manufacturerList;
        drugs = cvsAdmin.getDrugsCatalog().addDrugs();
        populateManufacturer();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtDrugName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        txtDrugNo = new javax.swing.JTextField();
        txtType = new javax.swing.JTextField();
        txtPrice = new javax.swing.JTextField();
        txtCompanyId = new javax.swing.JTextField();
        txtCompanyName = new javax.swing.JTextField();
        txtDescription = new javax.swing.JTextField();
        txtExpiryDate = new javax.swing.JTextField();
        txtManufacturedDate = new javax.swing.JTextField();
        txtContents = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblManuDrugs = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblManuName = new javax.swing.JTable();

        jLabel1.setText("Drug Name");

        jLabel5.setText("NEW DRUGS");

        btnAdd.setText("ADD DRUG");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnBack.setText("<<BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        txtDrugNo.setEditable(false);

        jLabel2.setText("Drug Number");

        jLabel3.setText("Type");

        jLabel4.setText("Price");

        jLabel6.setText("Company ID");

        jLabel7.setText("Company Name");

        jLabel8.setText("Description");

        jLabel9.setText("Expiry Date");

        jLabel10.setText("Manufactured Date");

        jLabel11.setText("Contents");

        tblManuDrugs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Drugs"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblManuDrugs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblManuDrugsMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblManuDrugs);
        if (tblManuDrugs.getColumnModel().getColumnCount() > 0) {
            tblManuDrugs.getColumnModel().getColumn(1).setHeaderValue("Drugs");
        }

        tblManuName.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblManuName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblManuNameMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblManuName);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(328, 328, 328)
                .addComponent(jLabel5)
                .addContainerGap(711, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(btnBack)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addGap(165, 165, 165)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAdd)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtDrugName, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                        .addComponent(txtDrugNo)
                        .addComponent(txtType)
                        .addComponent(txtPrice)
                        .addComponent(txtCompanyId)
                        .addComponent(txtCompanyName)
                        .addComponent(txtDescription)
                        .addComponent(txtExpiryDate)
                        .addComponent(txtManufacturedDate)
                        .addComponent(txtContents)))
                .addGap(254, 254, 254))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(70, 70, 70)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDrugName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDrugNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCompanyId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCompanyName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtExpiryDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(txtManufacturedDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtContents, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addGap(96, 96, 96)
                        .addComponent(btnAdd))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addComponent(btnBack)))
                .addContainerGap(247, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    public void populateManufacturer() {
//        jComboManyfacture.removeAllItems();
//        System.out.println("Outside");
//        for (Manufacturer manufacturer : manufacturerList.getManufacturerList()) {
//            System.out.println("inside" + manufacturer);
//
//            jComboManyfacture.addItem(manufacturer.getCompanyName());
//
//        }

        DefaultTableModel dtm = (DefaultTableModel) tblManuName.getModel();
        int rowCount = dtm.getRowCount();

        for (int x = rowCount - 1; x >= 0; x--) {
            dtm.removeRow(x);
        }

        for (Manufacturer manufacturer : manufacturerList.getManufacturerList()) {

            Object row[] = new Object[1];
            row[0] = manufacturer;

            dtm.addRow(row);

        }

    }

    public void populateDrugsTable() {

        int selectedRow1 = tblManuName.getSelectedRow();
        manufacturer6 = (Manufacturer) tblManuName.getValueAt(selectedRow1, 0);

        DefaultTableModel dtm1 = (DefaultTableModel) tblManuDrugs.getModel();
        int rowCount = dtm1.getRowCount();

        for (int x = rowCount - 1; x >= 0; x--) {
            dtm1.removeRow(x);
        }

        for (ManuDrugs manuDrugs : manufacturer6.getManuDrugsList().getManuDrugsList()) {

            Object row[] = new Object[1];
            row[0] = manuDrugs;

            dtm1.addRow(row);

        }

        int selectedRow2 = tblManuDrugs.getSelectedRow();
        manuDrugss = (ManuDrugs) tblManuDrugs.getValueAt(selectedRow1, 0);

    }


    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed

        if (txtType.getText().matches("[a-zA-Z]+")
                & txtPrice.getText().matches("[0-9]+")
                & txtCompanyId.getText().matches("[0-9]+")
                & txtDescription.getText().matches("[a-zA-Z]+")
                & txtExpiryDate.getText().matches("[a-zA-Z]+")
                & txtManufacturedDate.getText().matches("[a-zA-Z]+")) {

            drugs.setType(txtType.getText());
            drugs.setPrice(Integer.parseInt(txtPrice.getText()));

            drugs.setCompanyId(txtCompanyId.getText());
            drugs.setDescription(txtDescription.getText());
            drugs.setExpiryDate(txtExpiryDate.getText());
            drugs.setManufacturedDate(txtManufacturedDate.getText());
            drugs.setContents(txtContents.getText());
            
            drugs.setCompanyName(String.valueOf(manufacturer6));
            drugs.setDrugName(String.valueOf(manuDrugss));

            JOptionPane.showMessageDialog(null, "Drugs Added successfully!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Enter proper value!");
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        manageDrugs.populateTable();
        userProcessContainer.remove(this);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void tblManuNameMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblManuNameMousePressed
        // TODO add your handling code here:
        populateDrugsTable();
       // drugs.setCompanyName(String.valueOf(manufacturer6));
        txtCompanyName.setText(String.valueOf(manufacturer6));
    }//GEN-LAST:event_tblManuNameMousePressed

    private void tblManuDrugsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblManuDrugsMousePressed
        // TODO add your handling code here:
       // drugs.setDrugName(String.valueOf(manuDrugss));
        txtDrugName.setText(String.valueOf(manuDrugss));
    }//GEN-LAST:event_tblManuDrugsMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblManuDrugs;
    private javax.swing.JTable tblManuName;
    private javax.swing.JTextField txtCompanyId;
    private javax.swing.JTextField txtCompanyName;
    private javax.swing.JTextField txtContents;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtDrugName;
    private javax.swing.JTextField txtDrugNo;
    private javax.swing.JTextField txtExpiryDate;
    private javax.swing.JTextField txtManufacturedDate;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtType;
    // End of variables declaration//GEN-END:variables

}
