package UserInterface.CVSAdmin;

import Business.CvsAdmin;
import Business.Drugs;
import Business.Manufacturer;
import Business.ManufacturerList;
import Business.Stores;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class ManageDrugs extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private CvsAdmin cvsAdmin;
    private ManufacturerList manufacturerList;

    public ManageDrugs(JPanel upc, CvsAdmin cvsAdmin, ManufacturerList manufacturerList) {
        initComponents();
        this.userProcessContainer = upc;
        this.cvsAdmin = cvsAdmin;
        this.manufacturerList = manufacturerList;

        populateTable();
    }

    public void populateTable() {
        int rowCount = tblDrugs.getRowCount();
        DefaultTableModel dtm = (DefaultTableModel) tblDrugs.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
        for (Drugs drugs : cvsAdmin.getDrugsCatalog().getDrugsCatalog()) {
            Object row[] = new Object[4];
            row[0] = drugs;
            row[1] = drugs.getCompanyName();
            row[2] = drugs.getPrice();
            row[3] = drugs.getDrugNo();

            //adding row to table
            dtm.addRow(row);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblDrugs = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnAddStore = new javax.swing.JButton();
        btnViewStore = new javax.swing.JButton();
        txtSearchInventory = new javax.swing.JTextField();
        btnBrowsInventory = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        tblDrugs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drug Name", "Company Name", "Price", "Drug No"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblDrugs);

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnRemove.setText("Remove");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        btnAddStore.setText("Add Drug");
        btnAddStore.setToolTipText("");
        btnAddStore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddStoreActionPerformed(evt);
            }
        });

        btnViewStore.setText("View Drug");
        btnViewStore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewStoreActionPerformed(evt);
            }
        });

        btnBrowsInventory.setText("Brows Inventory");
        btnBrowsInventory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowsInventoryActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Enter Store ID");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(38, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSearchInventory, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBrowsInventory, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(77, 77, 77)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addComponent(btnBack)
                        .addGap(242, 242, 242)
                        .addComponent(btnRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAddStore, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnViewStore)))
                .addGap(90, 90, 90))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(149, 149, 149)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSearchInventory, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(29, 29, 29)
                        .addComponent(btnBrowsInventory, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 236, Short.MAX_VALUE)
                .addComponent(btnViewStore)
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack)
                    .addComponent(btnRemove)
                    .addComponent(btnAddStore))
                .addGap(171, 171, 171))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddStoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddStoreActionPerformed

        System.out.println("AddDrugs");
        AddDrugs addDrugs = new AddDrugs(userProcessContainer, cvsAdmin, this, manufacturerList);
        userProcessContainer.add("AddDrugs", addDrugs);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);
    }//GEN-LAST:event_btnAddStoreActionPerformed


    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        int selectedRow = tblDrugs.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a drug to be removed");
            return;
        }
        Drugs drugs = (Drugs) tblDrugs.getValueAt(selectedRow, 0);
        cvsAdmin.getDrugsCatalog().removeDrugs(drugs);
        populateTable();
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnViewStoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewStoreActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblDrugs.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "please select a row!");
            return;

        }
        Drugs d = (Drugs) tblDrugs.getValueAt(selectedRow, 0);
        ViewDrugs viewDrugs = new ViewDrugs(userProcessContainer, d);
        userProcessContainer.add("ViewDrugs", viewDrugs);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);
    }//GEN-LAST:event_btnViewStoreActionPerformed

    private void btnBrowsInventoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowsInventoryActionPerformed
        // TODO add your handling code here:
        System.out.println("Entered");
          int id = Integer.parseInt(txtSearchInventory.getText());
        
         if (txtSearchInventory.getText().matches("[1-9]+")) {

        if (id == 0) {
            JOptionPane.showMessageDialog(null, "Please enter Store ID");

        } else {

            Stores store = cvsAdmin.getStoresCatalog().searchStoreById(id);

            if (store != null) {
                System.out.println("if for SearchBarChartJPanel");

                DisplayInventoryItems inventoryItems = new DisplayInventoryItems(userProcessContainer, store);
                userProcessContainer.add("InventoryItems", inventoryItems);
                CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
                cardLayout.next(userProcessContainer);
            }
            else
            {
            JOptionPane.showMessageDialog(null, "This Store ID doesnt exists.");
            }


                                           
    }}
        
        else
       {
         JOptionPane.showMessageDialog(null, "Please enter proper Store ID");
        
        }
        
    }//GEN-LAST:event_btnBrowsInventoryActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddStore;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnBrowsInventory;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnViewStore;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDrugs;
    private javax.swing.JTextField txtSearchInventory;
    // End of variables declaration//GEN-END:variables

}
