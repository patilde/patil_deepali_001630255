package UserInterface.CVSAdmin;


import Business.Drugs;
import Business.Stores;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class ViewDrugs extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Drugs drugs;

    public ViewDrugs(JPanel userProcessContainer, Drugs drugs) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.drugs = drugs;
        lblDrugName.setText(drugs.getDrugName());
        populateTable();
    }

       public void populateTable() {
        int rowCount = tblViewDrug.getRowCount();
        DefaultTableModel dtm = (DefaultTableModel) tblViewDrug.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
        
            Object row[] = new Object[10];
            row[0] = drugs;
            row[1] = drugs.getDrugNo();
            row[2] = drugs.getPrice();
            row[3] = drugs.getCompanyName();
            row[4] = drugs.getCompanyId();
            row[5] = drugs.getType();
            row[6] = drugs.getManufacturedDate();
            row[7] = drugs.getExpiryDate();
            row[8] = drugs.getDescription();
            row[9] = drugs.getContents();
            

            //adding row to table
            dtm.addRow(row);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDrugName = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblViewDrug = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();

        lblDrugName.setText("jLabel1");

        tblViewDrug.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drug Name", "Drug No", "price", "Company Name", "Company Id", "Type", "ManufacturedDate", "ExpiryDate", "Description", "Contents"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblViewDrug);

        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(lblDrugName))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(469, 469, 469)
                        .addComponent(btnBack)))
                .addContainerGap(513, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1031, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lblDrugName)
                .addGap(30, 30, 30)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(btnBack)
                .addContainerGap(350, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDrugName;
    private javax.swing.JTable tblViewDrug;
    // End of variables declaration//GEN-END:variables
}
