package UserInterface.CVSAdmin;

import Business.CvsAdmin;
import Business.Drugs;
import Business.Stores;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class ManageStores extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private CvsAdmin cvsAdmin;

    public ManageStores(JPanel upc, CvsAdmin cvsAdmin) {
        initComponents();
        this.userProcessContainer = upc;
        this.cvsAdmin = cvsAdmin;
        populateTable();
    }

    public void populateTable() {
          int rowCount = tblAddStores.getRowCount();
        DefaultTableModel dtm = (DefaultTableModel) tblAddStores.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
        for (Stores stores : cvsAdmin.getStoresCatalog().getStoresCatalog()) {
            Object row[] = new Object[1];
            row[0] = stores;
            //adding row to table
            dtm.addRow(row);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblAddStores = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnAddStore = new javax.swing.JButton();
        btnViewStore = new javax.swing.JButton();

        tblAddStores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Store Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblAddStores);

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnRemove.setText("Remove");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        btnAddStore.setText("Add Store");
        btnAddStore.setToolTipText("");
        btnAddStore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddStoreActionPerformed(evt);
            }
        });

        btnViewStore.setText("View Store");
        btnViewStore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewStoreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addGap(29, 29, 29)
                        .addComponent(btnRemove)
                        .addGap(61, 61, 61)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnViewStore)
                            .addComponent(btnAddStore))))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack)
                    .addComponent(btnRemove)
                    .addComponent(btnViewStore))
                .addGap(18, 18, 18)
                .addComponent(btnAddStore)
                .addContainerGap(83, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddStoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddStoreActionPerformed
        AddStore addStore = new AddStore(userProcessContainer, cvsAdmin, this);
        userProcessContainer.add("addStore", addStore);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);


    }//GEN-LAST:event_btnAddStoreActionPerformed


    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
    int selectedRow = tblAddStores.getSelectedRow();
    if(selectedRow < 0){
        JOptionPane.showMessageDialog(null, "Please select a store name to be removed");
        return;
    }
    Stores stores = (Stores)tblAddStores.getValueAt(selectedRow, 0);
    cvsAdmin.getStoresCatalog().removeStores(stores);
    populateTable();
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnViewStoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewStoreActionPerformed
        // TODO add your handling code here:
         int selectedRow = tblAddStores.getSelectedRow();
        if(selectedRow < 0){
            JOptionPane.showMessageDialog(null, "please select a row!");
            return;
        
    }                                       
        Stores s = (Stores)tblAddStores.getValueAt(selectedRow, 0);
        ViewStore viewStore = new ViewStore(userProcessContainer, s);
        userProcessContainer.add("ViewStore", viewStore);
        CardLayout cardLayout = (CardLayout) userProcessContainer.getLayout();
        cardLayout.next(userProcessContainer);
    }//GEN-LAST:event_btnViewStoreActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddStore;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnViewStore;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblAddStores;
    // End of variables declaration//GEN-END:variables

}
