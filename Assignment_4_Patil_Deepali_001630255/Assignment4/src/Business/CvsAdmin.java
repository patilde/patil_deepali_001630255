/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class CvsAdmin {
    
    private String cvsAdminName;
    private DrugsCatalog drugsCatalog;
    private StoresCatalog storesCatalog;

    public CvsAdmin() {
        
        drugsCatalog = new DrugsCatalog();
        storesCatalog = new StoresCatalog();
    }

    public String getCvsAdminName() {
        return cvsAdminName;
    }

    public void setCvsAdminName(String cvsAdminName) {
        this.cvsAdminName = cvsAdminName;
    }

    public DrugsCatalog getDrugsCatalog() {
        return drugsCatalog;
    }

    public void setDrugsCatalog(DrugsCatalog drugsCatalog) {
        this.drugsCatalog = drugsCatalog;
    }

    public StoresCatalog getStoresCatalog() {
        return storesCatalog;
    }

    public void setStoresCatalog(StoresCatalog storesCatalog) {
        this.storesCatalog = storesCatalog;
    }
    
    
    
    
    
    
    
}
