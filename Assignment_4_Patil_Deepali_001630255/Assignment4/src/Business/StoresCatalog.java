/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class StoresCatalog {

    private ArrayList<Stores> storesCatalog;

    public StoresCatalog() {
        storesCatalog = new ArrayList<>();
    }

    public ArrayList<Stores> getStoresCatalog() {
        return storesCatalog;
    }

    public void setStoresCatalog(ArrayList<Stores> storesCatalog) {
        this.storesCatalog = storesCatalog;
    }

    
    public Stores addStores() {
        Stores stores = new Stores();
        this.storesCatalog.add(stores);
        return stores;
    }

    public void removeStores(Stores stores) {
        this.storesCatalog.remove(stores);
    }

     
        public Stores searchStoreById(int id) {
        for (Stores stores : storesCatalog) {

            if (id==stores.getStoreId()) {
                System.out.println("yes");
                return stores;
            }

        }
        System.out.println("No");

        return null;
    }

}
