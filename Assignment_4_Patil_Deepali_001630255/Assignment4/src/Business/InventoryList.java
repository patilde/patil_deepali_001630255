/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class InventoryList {

    private ArrayList<InventoryItems> inventoryList;

    public InventoryList() {
        inventoryList = new ArrayList<>();
    }

    public ArrayList<InventoryItems> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(ArrayList<InventoryItems> inventoryList) {
        this.inventoryList = inventoryList;
    }

    public InventoryItems addInventoryItems() {
        InventoryItems inventoryItems = new InventoryItems();
        this.inventoryList.add(inventoryItems);
        return inventoryItems;
    }

     public void removeItem(InventoryItems inventoryItem) {
        this.inventoryList.remove(inventoryItem);
    }
}
