/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class InventoryItems {

    private static int counter = 0;
    private Drugs drugs;
    private String itemName;
    private String expiryDate;
    private int inventoryDrugNo;
    private int threshold;
    private int quantity;

    public InventoryItems() {
       // drugs = new Drugs();
        counter++;
        this.inventoryDrugNo = counter;

    }

    public Drugs getDrugs() {
        return drugs;
    }

    public void setDrugs(Drugs drugs) {
        this.drugs = drugs;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        InventoryItems.counter = counter;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getInventoryDrugNo() {
        return inventoryDrugNo;
    }

    public void setInventoryDrugNo(int inventoryDrugNo) {
        this.inventoryDrugNo = inventoryDrugNo;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    
    @Override
    public String toString() {
        return itemName;
    }
    

}
