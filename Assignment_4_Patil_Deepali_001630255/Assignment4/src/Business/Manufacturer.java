/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class Manufacturer {
    
    private String companyName;
    private ManuDrugsList manuDrugsList;
    
     public Manufacturer()
    {
        manuDrugsList = new ManuDrugsList();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public ManuDrugsList getManuDrugsList() {
        return manuDrugsList;
    }

    public void setManuDrugsList(ManuDrugsList manuDrugsList) {
        this.manuDrugsList = manuDrugsList;
    }

        @Override
    public String toString() {
        return companyName;
    }
    
}
