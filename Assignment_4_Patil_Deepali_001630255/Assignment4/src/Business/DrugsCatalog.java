/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class DrugsCatalog {
    
    
    private ArrayList <Drugs> drugsCatalog;

    public DrugsCatalog() {
       drugsCatalog = new ArrayList<>();
    }

    public ArrayList<Drugs> getDrugsCatalog() {
        return drugsCatalog;
    }
    
    public Drugs addDrugs() {
        Drugs drugs = new Drugs();
        this.drugsCatalog.add(drugs);
        return drugs;
    }

    public void removeDrugs(Drugs drugs) {
        this.drugsCatalog.remove(drugs);
    }

    public Drugs searchDrugs(int id) {
        for (Drugs drugs : drugsCatalog) {
            if (drugs.getDrugNo() == id) {
                return drugs;
            }
        }
        return null;
    }
    
    
    
    
    
}
