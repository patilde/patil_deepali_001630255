/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class ManufacturerList {
    
     private ArrayList <Manufacturer> manufacturerList;

    public ManufacturerList() {
       manufacturerList = new ArrayList<>();
    }

    public ArrayList<Manufacturer> getManufacturerList() {
        return manufacturerList;
    }

    public void setManufacturerList(ArrayList<Manufacturer> manufacturerList) {
        this.manufacturerList = manufacturerList;
    }

    
    public Manufacturer addManufacturer() {
        Manufacturer manufacturer = new Manufacturer();
        this.manufacturerList.add(manufacturer);
        return manufacturer;
    }
    
    
}
