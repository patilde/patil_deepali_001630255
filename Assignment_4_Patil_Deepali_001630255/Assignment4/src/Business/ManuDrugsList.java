/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class ManuDrugsList {

    private ArrayList<ManuDrugs> manuDrugsList;

    public ManuDrugsList() {
        manuDrugsList = new ArrayList<>();
    }

    public ArrayList<ManuDrugs> getManuDrugsList() {
        return manuDrugsList;
    }

    public void setManuDrugsList(ArrayList<ManuDrugs> manuDrugsList) {
        this.manuDrugsList = manuDrugsList;
    }
    
        public ManuDrugs addManuDrugs() {
        ManuDrugs manuDrugs = new ManuDrugs();
        this.manuDrugsList.add(manuDrugs);
        return manuDrugs;
    }
    
    

}
