/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class StoreAdmin {
    
    private String storeAdminName;
    private Stores stores;

    public StoreAdmin() {
        
        stores = new Stores();
    }

    public String getStoreAdminName() {
        return storeAdminName;
    }

    public void setStoreAdminName(String storeAdminName) {
        this.storeAdminName = storeAdminName;
    }

    public Stores getStores() {
        return stores;
    }

    public void setStores(Stores stores) {
        this.stores = stores;
    }
    
    
    
    
    
}
