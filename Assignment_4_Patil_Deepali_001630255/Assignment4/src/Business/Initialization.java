/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class Initialization {

    public ManufacturerList populateManu() {

        ManufacturerList mL = new ManufacturerList();

        Manufacturer m1 = mL.addManufacturer();
        m1.setCompanyName("S1");

        ManuDrugs mD1 = m1.getManuDrugsList().addManuDrugs();
        mD1.setDrugName("D1");

        ManuDrugs mD10 = m1.getManuDrugsList().addManuDrugs();
        mD10.setDrugName("D10");

        Manufacturer m2 = mL.addManufacturer();
        m2.setCompanyName("S2");

        ManuDrugs mD2 = m2.getManuDrugsList().addManuDrugs();
        mD2.setDrugName("D2");

        ManuDrugs mD11 = m2.getManuDrugsList().addManuDrugs();
        mD11.setDrugName("D11");

        Manufacturer m3 = mL.addManufacturer();
        m3.setCompanyName("S3");

        ManuDrugs mD3 = m3.getManuDrugsList().addManuDrugs();
        mD3.setDrugName("D3");

        ManuDrugs mD12 = m3.getManuDrugsList().addManuDrugs();
        mD12.setDrugName("D12");
        
        Manufacturer m4 = mL.addManufacturer();
        m4.setCompanyName("S4");

        ManuDrugs mD4 = m4.getManuDrugsList().addManuDrugs();
        mD4.setDrugName("D4");

        ManuDrugs mD13 = m4.getManuDrugsList().addManuDrugs();
        mD13.setDrugName("D13");

        //  System.out.println("First Product" +s1.getSupplierName());
        for (Manufacturer manufacturer : mL.getManufacturerList()) {
            System.out.println("Manyfacturer :" + manufacturer.getCompanyName());

            for (ManuDrugs manuDrugs : manufacturer.getManuDrugsList().getManuDrugsList()) {
                System.out.println("Drug :" + manuDrugs.getDrugName());
            }
        }
        return mL;
    }

    public CvsAdmin populateDrugs() {

        CvsAdmin cvs = new CvsAdmin();
        
        DrugsCatalog dC = cvs.getDrugsCatalog();
        
        Drugs d1 = dC.addDrugs();
        d1.setDrugName("D1");
        d1.setCompanyName("Diamond");
        d1.setCompanyId("1");
        d1.setContents("D1Contents");
        d1.setDescription("D1Description");
        d1.setDrugNo(Integer.valueOf("1"));
        d1.setExpiryDate("12Nov2013");
        d1.setManufacturedDate("12Nov2012");
        d1.setPrice(Integer.valueOf("100"));
        d1.setType("20");//Quantity

        Drugs d2 = dC.addDrugs();
        d2.setDrugName("D2");
        d2.setCompanyName("Medical2");
        d2.setCompanyId("2");
        d2.setContents("D2Contents");
        d2.setDescription("D2Description");
        d2.setDrugNo(Integer.valueOf("2"));
        d2.setExpiryDate("15Nov2014");
        d2.setManufacturedDate("15Nov2013");
        d2.setPrice(Integer.valueOf("200"));
        d2.setType("20");//Quantity

        Drugs d3 = dC.addDrugs();
        d3.setDrugName("D3");
        d3.setCompanyName("Pharmacy");
        d3.setCompanyId("3");
        d3.setContents("D3Contents");
        d3.setDescription("D3Description");
        d3.setDrugNo(Integer.valueOf("3"));
        d3.setExpiryDate("15Dec2015");
        d3.setManufacturedDate("15Dec2014");
        d3.setPrice(Integer.valueOf("300"));
        d3.setType("20");//Quantity

        StoresCatalog sC = cvs.getStoresCatalog();
        Stores s1 = sC.addStores();
        s1.setStoreName("S1");
        s1.setStoreId(Integer.parseInt("1"));
        s1.setStoreEmail("s1Email");
        s1.setStoreAddress("s1Address");
        s1.setStoreTelephone("s1StoreTelephone");
        s1.setStoreWebsite("s1StoreWebsite");

        Stores s2 = sC.addStores();
        s2.setStoreName("S2");
        s2.setStoreId(Integer.valueOf(2));
        s2.setStoreEmail("s2Email");
        s2.setStoreAddress("s2Address");
        s2.setStoreTelephone("s2StoreTelephone");
        s2.setStoreWebsite("s2StoreWebsite");

        Stores s3 = sC.addStores();
        s3.setStoreName("S3");
        s3.setStoreId(Integer.valueOf(3));
        s3.setStoreEmail("s3Email");
        s3.setStoreAddress("s3Address");
        s3.setStoreTelephone("s3StoreTelephone");
        s3.setStoreWebsite("s3StoreWebsite");

        InventoryItems i1 = s1.getInventoryList().addInventoryItems();
        i1.setExpiryDate("D1ExpiryDate");
        i1.setInventoryDrugNo(Integer.valueOf(1));
        i1.setItemName("D1");
        i1.setQuantity(Integer.valueOf("10"));
        i1.setThreshold(Integer.valueOf("2"));
        
        InventoryItems i4 = s1.getInventoryList().addInventoryItems();
        i4.setExpiryDate("D2ExpiryDate");
        i4.setInventoryDrugNo(Integer.valueOf(2));
        i4.setItemName("D2");
        i4.setQuantity(Integer.valueOf(0));
        i4.setThreshold(Integer.valueOf(2));

        InventoryItems i2 = s2.getInventoryList().addInventoryItems();
        i2.setExpiryDate("I2ExpiryDate");
        i2.setInventoryDrugNo(Integer.valueOf(2));
        i2.setItemName("D2");
        i2.setQuantity(Integer.valueOf("6"));
        i2.setThreshold(Integer.valueOf("2"));

        InventoryItems i3 = s3.getInventoryList().addInventoryItems();
        i3.setExpiryDate("I3ExpiryDate");
        i3.setInventoryDrugNo(Integer.valueOf(3));
        i3.setItemName("D3");
        i3.setQuantity(Integer.valueOf("15"));
        i3.setThreshold(Integer.valueOf("3"));

        return cvs;
    }

}
