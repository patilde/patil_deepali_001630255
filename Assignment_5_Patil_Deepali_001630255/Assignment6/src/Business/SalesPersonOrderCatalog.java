/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class SalesPersonOrderCatalog {

    private ArrayList<Order> oPC;

    public SalesPersonOrderCatalog() {
        oPC = new ArrayList<>();
    }

    public ArrayList<Order> getoPC() {
        return oPC;
    }

    public void setoPC(ArrayList<Order> oPC) {
        this.oPC = oPC;
    }

    public Order addSalesPersonOrder(Order o) {
        oPC.add(o);
        return o;
    }

}
