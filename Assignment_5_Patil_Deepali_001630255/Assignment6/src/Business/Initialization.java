/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class Initialization {
//    public CustomerDirectory populateCustomer() {
//    CustomerDirectory cDc=new CustomerDirectory();
//    
//    
//    return cDc;
//    }

    public ProductCatalog populateProducts() {

        ProductCatalog pc = new ProductCatalog();
        Product pd = pc.addProduct();
        pd.setProdName("XROX1");
        pd.setTargetprice(Integer.parseInt("300"));
        pd.setCeilingPrice(Integer.parseInt("500"));
        pd.setFloorPrice(Integer.parseInt("100"));
        pd.setAvail(100);
        pd.setOld_value(100);
        pd.setModelNumber(1);

        Product pd2 = pc.addProduct();
        pd2.setProdName("XROX2");
        pd2.setTargetprice(Integer.parseInt("400"));
        pd2.setCeilingPrice(Integer.parseInt("600"));
        pd2.setFloorPrice(Integer.parseInt("200"));
        pd2.setAvail(200);
        pd2.setOld_value(200);
        pd2.setModelNumber(2);

        Product pd3 = pc.addProduct();
        pd3.setProdName("XROX3");
        pd3.setTargetprice(Integer.parseInt("500"));
        pd3.setCeilingPrice(Integer.parseInt("700"));
        pd3.setFloorPrice(Integer.parseInt("300"));
        pd3.setAvail(150);
        pd3.setOld_value(150);
        pd3.setModelNumber(3);

        Product pd4 = pc.addProduct();
        pd4.setProdName("XROX4");
        pd4.setTargetprice(Integer.parseInt("600"));
        pd4.setCeilingPrice(Integer.parseInt("800"));
        pd4.setFloorPrice(Integer.parseInt("400"));
        pd4.setAvail(170);
        pd4.setOld_value(170);
        pd4.setModelNumber(4);

        return pc;
    }

}
