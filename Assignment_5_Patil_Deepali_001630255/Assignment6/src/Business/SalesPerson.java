/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class SalesPerson implements Comparable {

    private String salesPersons_name;
    private SalesPersonOrderCatalog salesPersonOrderCatalog;
    private int products_sold;
    private double commision;
    private int aboveTarget;
    private int belowTarget;

    public SalesPerson() {

        salesPersonOrderCatalog = new SalesPersonOrderCatalog();
    }

    public String getSalesPersons_name() {
        return salesPersons_name;
    }

    public void setSalesPersons_name(String salesPersons_name) {
        this.salesPersons_name = salesPersons_name;
    }

    public SalesPersonOrderCatalog getSalesPersonOrderCatalog() {
        return salesPersonOrderCatalog;
    }

    public void setSalesPersonOrderCatalog(SalesPersonOrderCatalog salesPersonOrderCatalog) {
        this.salesPersonOrderCatalog = salesPersonOrderCatalog;
    }

    public int getProducts_sold() {
        return products_sold;
    }

    public void setProducts_sold(int products_sold) {
        this.products_sold = products_sold;
    }

    public double getCommision() {
        return commision;
    }

    public void setCommision(double commision) {
        this.commision = commision;
    }

    public int getAboveTarget() {
        return aboveTarget;
    }

    public void setAboveTarget(int aboveTarget) {
        this.aboveTarget = aboveTarget;
    }

    public int getBelowTarget() {
        return belowTarget;
    }

    public void setBelowTarget(int belowTarget) {
        this.belowTarget = belowTarget;
    }

    @Override
    public String toString() {
        return String.valueOf(products_sold);
    }

    @Override
    public int compareTo(Object sp) {
        System.out.println("LLLLLLLLLLLLLL");
        int abc = ((SalesPerson) sp).getProducts_sold();
        return abc - this.products_sold;
    }

}
