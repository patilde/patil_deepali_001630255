/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class CustomerOrderCatalog {

    private ArrayList<Order> cOC;

    public CustomerOrderCatalog() {
        cOC = new ArrayList<>();
    }

    public ArrayList<Order> getcOC() {
        return cOC;
    }

    public void setcOC(ArrayList<Order> cOC) {
        this.cOC = cOC;
    }

    public Order addCustomerOrder(Order o) {
        cOC.add(o);
        return o;
    }

}
