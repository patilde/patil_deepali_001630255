/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author DeepaliP
 */
public class Customer  implements Comparable{

    private String firstName;
    private int SSN;
    private CustomerOrderCatalog customerOrderCatalog;
    private int items_purchased;

    public Customer() {
        customerOrderCatalog = new CustomerOrderCatalog();
    }
  
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getSSN() {
        return SSN;
    }

    public void setSSN(int SSN) {
        this.SSN = SSN;
    }

    public CustomerOrderCatalog getCustomerOrderCatalog() {
        return customerOrderCatalog;
    }

    public void setCustomerOrderCatalog(CustomerOrderCatalog customerOrderCatalog) {
        this.customerOrderCatalog = customerOrderCatalog;
    }

    public int getItems_purchased() {
        return items_purchased;
    }

    public void setItems_purchased(int items_purchased) {
        this.items_purchased = items_purchased;
    }
    
    

    @Override
    public String toString() {
        return String.valueOf(items_purchased);
    }
    
    
    @Override
    public int compareTo(Object cus) {
        System.out.println("inside customer override");
        int cust = ((Customer) cus).getItems_purchased();
        return cust-this.getItems_purchased();
    }
}
