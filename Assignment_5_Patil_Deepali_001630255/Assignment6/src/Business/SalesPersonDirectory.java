/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DeepaliP
 */
public class SalesPersonDirectory {

    private List<SalesPerson> salesPersonDirectory;

    public SalesPersonDirectory() {

        salesPersonDirectory = new ArrayList<SalesPerson>();
    }

    public List<SalesPerson> getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(List<SalesPerson> salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }

    public SalesPerson addSalesPerson() {
        SalesPerson s = new SalesPerson();
        salesPersonDirectory.add(s);
        return s;
    }

    public void removeSalesPerson(SalesPerson s) {
        salesPersonDirectory.remove(s);
    }

    public SalesPerson searchSalesPerson(String keyword) {
        for (SalesPerson salesPerson : salesPersonDirectory) {
            if (salesPerson.getSalesPersons_name().equals(keyword)) {
                return salesPerson;
            }
        }
        return null;
    }

}
