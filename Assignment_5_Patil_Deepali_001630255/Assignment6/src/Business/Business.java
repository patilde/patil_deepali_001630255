package Business;

/**
 * @author Hechen Gao
 * @contact gao.h@husky.neu.edu
 * @date Oct 2, 2014
 */
public class Business {

    private SalesPersonDirectory salesPersonDirectory;
    private MasterOrderCatalog masterOrderCatalog;

    public Business() {
        salesPersonDirectory = new SalesPersonDirectory();
        masterOrderCatalog = new MasterOrderCatalog();
    }

    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }

}
