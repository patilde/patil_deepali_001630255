/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author DeepaliP
 */
public class CustomerDirectory {
    
    private ArrayList<Customer> customerDirectory;
    
    public CustomerDirectory() {
        customerDirectory = new ArrayList<>();
    }

    public ArrayList<Customer> getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> customerDirectory) {
        this.customerDirectory = customerDirectory;
    }
    
     public Customer addCustomer(){
        Customer c = new Customer();
        customerDirectory.add(c);
        return c;
    }
     
      public Customer searchCustomer(int id){
        for (Customer customer : customerDirectory) {
            if(customer.getSSN()==id){
                return customer;
                
            }
           
        }
        return null;
    }
    
    
}
