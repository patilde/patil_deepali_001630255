/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class Product implements Comparable {

    private String prodName;
    private int targetprice;
    private int floorPrice;
    private int ceilingPrice;
    private int price;
    private int modelNumber;
    private int avail;
    private int old_value;
    private int subtracted_value;

    private static int count = 0;

   @Override
    public String toString() {
        return String.valueOf(subtracted_value); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int getSubtracted_value() {
        return subtracted_value;
    }

    public void setSubtracted_value(int subtracted_value) {
        this.subtracted_value = subtracted_value;
    }

    public int getOld_value() {
        return old_value;
    }

    public void setOld_value(int old_value) {
        this.old_value = old_value;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAvail() {
        return avail;
    }

    public void setAvail(int avail) {
        this.avail = avail;
    }

    public Product() {
        count++;
        modelNumber = count;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getTargetprice() {
        return targetprice;
    }

    public void setTargetprice(int targetprice) {
        this.targetprice = targetprice;
    }

    public int getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(int floorPrice) {
        this.floorPrice = floorPrice;
    }

    public int getCeilingPrice() {
        return ceilingPrice;
    }

    public void setCeilingPrice(int ceilingPrice) {
        this.ceilingPrice = ceilingPrice;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    
     

      @Override
    public int compareTo(Object prod) {
        System.out.println("inside product override");
        int prodt = ((Product) prod).subtracted_value;
        return prodt-this.getSubtracted_value();
    }
}
