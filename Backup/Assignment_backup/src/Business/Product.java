/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.JFormattedTextField;

/**
 *
 * @author DeepaliP
 */
public class Product {
    
    private String firstName; 
    private String lastName; 
    private String middleName; 
    private String dateOfBirth; 
    private String streetAddress; 
    private String town;
    private String zipCode;
    private String occupation;
    private String emailAddress;
    private String areaCodeOfPhoneNumber;
    private String phoneNumber;
    DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
    JFormattedTextField dateTextField = new JFormattedTextField(format);

    public JFormattedTextField getDateTextField() {
        return dateTextField;
    }

    public void setDateTextField(JFormattedTextField dateTextField) {
        this.dateTextField = dateTextField;
    }

    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAreaCodeOfPhoneNumber() {
        return areaCodeOfPhoneNumber;
    }

    public void setAreaCodeOfPhoneNumber(String areaCodeOfPhoneNumber) {
        this.areaCodeOfPhoneNumber = areaCodeOfPhoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    

    
    
}
