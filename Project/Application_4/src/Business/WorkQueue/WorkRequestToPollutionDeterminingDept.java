/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author DeepaliP
 */
public class WorkRequestToPollutionDeterminingDept {

    private String message;
    private UserAccount sender;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private ArrayList<Integer> listt1;
    private ArrayList<String> listt2;

//    public WorkRequestToPollutionDeterminingDept(ArrayList list1,ArrayList list2) {
//        requestDate = new Date();
//        this.listt1 = list1;
//        this. listt2 = list2;
//    }

    public WorkRequestToPollutionDeterminingDept() {
        requestDate = new Date();
    }

    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

    public ArrayList<Integer> getListt1() {
        return listt1;
    }

    public void setListt1(ArrayList<Integer> listt1) {
        this.listt1 = listt1;
    }

    public ArrayList<String> getListt2() {
        return listt2;
    }

    public void setListt2(ArrayList<String> listt2) {
        this.listt2 = listt2;
    }
    

    @Override
    public String toString() {
        return message;
    }

}
