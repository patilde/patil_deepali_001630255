/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkQueueTrafficControlRoom;
import Business.WorkQueue.WorkRequestToPollutionDeterminingDept;

/**
 *
 * @author DeepaliP
 */
public class UserAccount {

    private String username;
    private String password;
    private Employee employee;
    private Role role;
    private WorkQueue workQueue;
    private WorkQueueTrafficControlRoom workQueueTrafficControlRoom;
    private WorkRequestToPollutionDeterminingDept workRequestToPollutionDeterminingDept;

    public UserAccount() {
        workQueue = new WorkQueue();
        workQueueTrafficControlRoom = new WorkQueueTrafficControlRoom();
        workRequestToPollutionDeterminingDept = new WorkRequestToPollutionDeterminingDept();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public WorkQueueTrafficControlRoom getWorkQueueTrafficControlRoom() {
        return workQueueTrafficControlRoom;
    }

    public void setWorkQueueTrafficControlRoom(WorkQueueTrafficControlRoom workQueueTrafficControlRoom) {
        this.workQueueTrafficControlRoom = workQueueTrafficControlRoom;
    }

    public WorkRequestToPollutionDeterminingDept getWorkRequestToPollutionDeterminingDept() {
        return workRequestToPollutionDeterminingDept;
    }

    public void setWorkRequestToPollutionDeterminingDept(WorkRequestToPollutionDeterminingDept workRequestToPollutionDeterminingDept) {
        this.workRequestToPollutionDeterminingDept = workRequestToPollutionDeterminingDept;
    }
    

    @Override
    public String toString() {
        return username;
    }

}
