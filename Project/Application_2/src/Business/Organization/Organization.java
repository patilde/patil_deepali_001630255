/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkQueueToPollutionDeterminingDept;
import Business.WorkQueue.WorkQueueTrafficControlRoom;
import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private WorkQueueTrafficControlRoom workQueueTrafficControlRoom;
    private WorkQueueToPollutionDeterminingDept workQueueToPollutionDeterminingDept;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;

    public enum Type {

        Admin("Admin Organization"),
        TrafficControlRoom("TrafficControlRoom"),
        //FacilityControlRoom("FacilityControlRoom Organization"),
        TrafficManagementPatrol("TrafficManagement"),
        RegularMaintainance("RegularMaintainance"),
        PollutionDeterminationDept("PollutionDeterminationDept"),
        TrafficDeterminingDept("TrafficDeterminingDepts"),
        Citizens("Citizens");

        // RegistrationOffice("RegistrationOffice Oraganization");
        private String value;

        private Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        workQueueTrafficControlRoom = new WorkQueueTrafficControlRoom();
        workQueueToPollutionDeterminingDept = new WorkQueueToPollutionDeterminingDept();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public WorkQueueTrafficControlRoom getWorkQueueTrafficControlRoom() {
        return workQueueTrafficControlRoom;
    }

    public void setWorkQueueTrafficControlRoom(WorkQueueTrafficControlRoom workQueueTrafficControlRoom) {
        this.workQueueTrafficControlRoom = workQueueTrafficControlRoom;
    }

    public WorkQueueToPollutionDeterminingDept getWorkQueueToPollutionDeterminingDept() {
        return workQueueToPollutionDeterminingDept;
    }

    public void setWorkQueueToPollutionDeterminingDept(WorkQueueToPollutionDeterminingDept workQueueToPollutionDeterminingDept) {
        this.workQueueToPollutionDeterminingDept = workQueueToPollutionDeterminingDept;
    }

    @Override
    public String toString() {
        return name;
    }

}
