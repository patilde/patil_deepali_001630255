/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.TraficControlRoomRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.CitizenOrganisation;
import Business.Organization.Citizens;
import Business.Organization.Organization;
import Business.Organization.TraficControlRoomOrganisation;
import Business.Organization.TraficManagementPatrolOrganisation;

import Business.UserAccount.UserAccount;

import Business.WorkQueue.TrafficControlToPatrol;
import Business.WorkQueue.TrafficDeterToPollutionDeterDept;
import Business.WorkQueue.TrafficDeterminingDeptToTrafficControl;

import Business.WorkQueue.WorkRequest;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import static userinterface.TraficControlRoomRole.MailSender.emailBody;
import static userinterface.TraficControlRoomRole.MailSender.emailSubject;

/**
 *
 * @author DeepaliP
 */
public class TraficControlRoomWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private TraficControlRoomOrganisation organization;
//    private TraficControlRoomOrganisation orga;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private ArrayList<String> list3;
    private String sum = " ";
    String a = " ";
    String b = " ";
    String result = "";
    String x = " ";
    String y = " ";

    /**
     * Creates new form PollutionDeterminingDeptWorkAreaJPanel
     */
    public TraficControlRoomWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, TraficControlRoomOrganisation organization, Enterprise enterprise) {
        initComponents();

        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.userAccount = account;
        valueLabel.setText(enterprise.getName());
        list3 = new ArrayList<String>();
        populateRequestTable();
        populateRequestTable1();
        populateRequestTable2();

    }

    public void populateRequestTable() {
        DefaultTableModel model = (DefaultTableModel) tbltrafficControlRoom.getModel();
        //TableColumn sportColumn1 = tbltrafficControlRoom.getColumnModel().getColumn(0);
        TableColumn sportColumn2 = tbltrafficControlRoom.getColumnModel().getColumn(1);
        TableColumn sportColumn3 = tbltrafficControlRoom.getColumnModel().getColumn(2);

        JComboBox comboBox = new JComboBox();

        comboBox.addItem("Allston");
        //comboBox.setSelectedItem("Snowboarding");
        comboBox.addItem("Brighton");
        comboBox.addItem("Fenway Kenmore");
        comboBox.addItem("Mission Hill");
        comboBox.addItem("Jamaica PlainRoxbury");
        comboBox.addItem("Downtown");
        comboBox.setSelectedItem("CharlesTown");
        comboBox.setSelectedItem("Back Bay");
        comboBox.setSelectedItem("Dorchester");
        comboBox.setSelectedItem("South End");
        comboBox.setSelectedItem("Bay Village");
        comboBox.setSelectedItem("South Boston");
        comboBox.setSelectedItem("East Boston");
        comboBox.setSelectedItem("Hyde Park");

        sportColumn3.setCellEditor(new DefaultCellEditor(comboBox));

        tbltrafficControlRoom.setRowHeight(0, 40);
        tbltrafficControlRoom.setRowHeight(1, 40);
        tbltrafficControlRoom.setRowHeight(2, 40);
        tbltrafficControlRoom.setRowHeight(3, 40);
    }

    public void populateRequestTable1() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable1.getModel();

        model.setRowCount(0);
        for (WorkRequest wr : userAccount.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[3];
            row[0] = wr.getMessage();
            row[1] = wr.getReceiver();
            row[2] = wr.getStatus();
            //String result = ((TrafficControlToPatrol) wr).getTestResult();
            //row[3] = result == null ? "Waiting" : result;
            // row[3] = "Processed";

            model.addRow(row);
        }
    }

    public void populateRequestTable2() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);
        for (WorkRequest wr1 : userAccount.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[3];
            row[0] = wr1.getMessage();
            row[1] = wr1.getReceiver();
            row[2] = wr1.getStatus();
            // String result = ((TrafficDeterminingDeptToTrafficControl) wr1).getTestResult();
            // row[3] = result == null ? "Waiting" : result;
            //row[3] = "Processed";

            model.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        requestTestJButton = new javax.swing.JButton();
        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbltrafficControlRoom = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        btnSendEmail = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        workRequestJTable1 = new javax.swing.JTable();
        btnProcessRequest = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnSendSMS = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(new java.awt.Color(204, 204, 204));

        requestTestJButton.setBackground(new java.awt.Color(204, 204, 255));
        requestTestJButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        requestTestJButton.setText("SEND REQUEST");
        requestTestJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestTestJButtonActionPerformed(evt);
            }
        });

        enterpriseLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        enterpriseLabel.setText("ENTERPRISE");

        tbltrafficControlRoom.setBackground(new java.awt.Color(204, 204, 255));
        tbltrafficControlRoom.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbltrafficControlRoom.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Accidents", null, null},
                {"Natural Calamities", null, null},
                {"Weather Conditions", null, null},
                {"Maintainence Issue", null, null}
            },
            new String [] {
                "Situations", "Select", "Places"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Boolean.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tbltrafficControlRoom);
        if (tbltrafficControlRoom.getColumnModel().getColumnCount() > 0) {
            tbltrafficControlRoom.getColumnModel().getColumn(2).setResizable(false);
        }

        workRequestJTable.setBackground(new java.awt.Color(204, 204, 255));
        workRequestJTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Message", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);

        btnSendEmail.setBackground(new java.awt.Color(204, 204, 255));
        btnSendEmail.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSendEmail.setText("SEND EMAIL");
        btnSendEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendEmailActionPerformed(evt);
            }
        });

        workRequestJTable1.setBackground(new java.awt.Color(204, 204, 255));
        workRequestJTable1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        workRequestJTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Message", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(workRequestJTable1);

        btnProcessRequest.setBackground(new java.awt.Color(204, 204, 255));
        btnProcessRequest.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnProcessRequest.setText(" PROCESS REQUEST");
        btnProcessRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessRequestActionPerformed(evt);
            }
        });

        jLabel2.setBackground(new java.awt.Color(204, 204, 255));
        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel2.setText("TRAFFIC CONTROL ROOM JPANEL");

        btnSendSMS.setBackground(new java.awt.Color(204, 204, 255));
        btnSendSMS.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSendSMS.setText("SEND SMS");
        btnSendSMS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendSMSActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane3)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 844, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnSendEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                    .addComponent(btnSendSMS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(requestTestJButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(56, 56, 56)
                                .addComponent(btnProcessRequest))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(278, 278, 278)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(111, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(66, 66, 66)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(89, 89, 89)
                                .addComponent(btnSendEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(33, 33, 33)
                                .addComponent(btnSendSMS, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(37, 37, 37)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(357, 357, 357)
                        .addComponent(requestTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(btnProcessRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(285, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void requestTestJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestTestJButtonActionPerformed
        String a = " ";
        String b = " ";
        String sum = " ";
        try {

            for (int i = 0; i < tbltrafficControlRoom.getRowCount(); i++) {
                boolean isChecked = (Boolean) tbltrafficControlRoom.getValueAt(i, 1);

                if (isChecked) { 
                    a += " " + (String) tbltrafficControlRoom.getValueAt(i, 0);
                    //System.out.println("values[0]--->" + a);
                }
                b += "at " + tbltrafficControlRoom.getModel().getValueAt(i, 2);
                sum += a + b;
                // System.out.println("val0]--->" + b);

            }

        } catch (Exception e) {
            System.out.println("KIKIKI");
        }

        TrafficControlToPatrol req1 = new TrafficControlToPatrol();
        req1.setMessage(sum);
        req1.setSender(userAccount);
        req1.setStatus("Sent");

        Organization org = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (organization instanceof TraficManagementPatrolOrganisation) {
                org = organization;
                break;
            }
        }
        if (org != null) {
            org.getWorkQueue().getWorkRequestList().add(req1);
            userAccount.getWorkQueue().getWorkRequestList().add(req1);
        }
        populateRequestTable1();
        JOptionPane.showMessageDialog(null, "Request sent successfully");

    }//GEN-LAST:event_requestTestJButtonActionPerformed

    private void btnProcessRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessRequestActionPerformed
        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        TrafficDeterminingDeptToTrafficControl reqs = (TrafficDeterminingDeptToTrafficControl) workRequestJTable.getValueAt(selectedRow, 0);
        reqs.setTestResult("Done");
        reqs.setStatus("Completed");
        populateRequestTable2();
    }//GEN-LAST:event_btnProcessRequestActionPerformed

    private void btnSendEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendEmailActionPerformed

        CitizenOrganisation ho;

        for (Organization or : enterprise.getOrganizationDirectory().getOrganizationList()) {

            if (or instanceof CitizenOrganisation) {
                ho = (CitizenOrganisation) or;
                for (Citizens c : ho.cD.getCitizensdirectory()) {
                    list3.add(c.getEmail_id());
                    //tele_no= new String[organ.getCitizenDirectory().getCitizensdirectory().size()];
                }
                String email_id[] = new String[list3.size()];

                for (int k = 0; k < list3.size(); k++) {

                    email_id[k] = list3.get(k);
                    userinterface.TraficManagementPatrolRole.MailSender mailSender = new userinterface.TraficManagementPatrolRole.MailSender(email_id[k], emailSubject, emailBody);
                    System.out.println("Control rool Email" + email_id[k]);
                    JOptionPane.showMessageDialog(null, "Email sent successfully");
                }

            }
        }

    }//GEN-LAST:event_btnSendEmailActionPerformed

    private void btnSendSMSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendSMSActionPerformed
        CitizenOrganisation ho;
        System.out.println("Hello11");
        for (Organization or : enterprise.getOrganizationDirectory().getOrganizationList()) {

            if (or instanceof CitizenOrganisation) {
                ho = (CitizenOrganisation) or;
                if (ho.cD.getCitizensdirectory().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Please add Citizens");
                } else {
                    for (Citizens c : ho.cD.getCitizensdirectory()) {
                        list3.add(c.getTelephone_no());
                        //tele_no= new String[organ.getCitizenDirectory().getCitizensdirectory().size()];
                    }
                    String tele_no[] = new String[list3.size()];

                    for (int k = 0; k < list3.size(); k++) {

                        tele_no[k] = list3.get(k);
                        System.out.println("tele_no[k]---->" + tele_no[k]);

                        try {
                            // Construct data
                            String data = "";
                            /*
                             * Note the suggested encoding for certain parameters, notably
                             * the username, password and especially the message.  ISO-8859-1
                             * is essentially the character set that we use for message bodies,
                             * with a few exceptions for e.g. Greek characters.  For a full list,
                             * see:  http://developer.bulksms.com/eapi/submission/character-encoding/
                             */
                            data += "username=" + URLEncoder.encode("Deepali2407", "ISO-8859-1");
                            data += "&password=" + URLEncoder.encode("Deepali2407", "ISO-8859-1");
                            data += "&message=" + URLEncoder.encode("Tesing", "ISO-8859-1");
                            data += "&want_report=1";
                            data += "&msisdn=" + tele_no[k];

                            URL url = new URL("http://usa.bulksms.com/eapi/submission/send_sms/2/2.0");

                            URLConnection conn = url.openConnection();
                            conn.setDoOutput(true);
                            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                            wr.write(data);
                            wr.flush();

                            // Get the response
                            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            String line;
                            while ((line = rd.readLine()) != null) {
                                // Print the response output...
                                System.out.println(line);
                            }
                            JOptionPane.showMessageDialog(null, "Message Sent Successfully");
                            wr.close();
                            rd.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("Something Went Wrong!!");
                        }
                    }
                }

            }
        }

    }//GEN-LAST:event_btnSendSMSActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnProcessRequest;
    private javax.swing.JButton btnSendEmail;
    private javax.swing.JButton btnSendSMS;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JButton requestTestJButton;
    private javax.swing.JTable tbltrafficControlRoom;
    private javax.swing.JLabel valueLabel;
    private javax.swing.JTable workRequestJTable;
    private javax.swing.JTable workRequestJTable1;
    // End of variables declaration//GEN-END:variables
}
