/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class WorkQueueToPollutionDeterminingDept {

    private ArrayList<WorkRequestToPollutionDeterminingDept> workRequestL;

    public WorkQueueToPollutionDeterminingDept() {
        workRequestL = new ArrayList<>();
    }

    public ArrayList<WorkRequestToPollutionDeterminingDept> getWorkRequestList() {
        return workRequestL;
    }
}
