/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class WorkQueueTrafficControlRoom {

    private ArrayList<WorkRequestToTrafficControlRoom> workRequestList;

    public WorkQueueTrafficControlRoom() {
        workRequestList = new ArrayList<>();
    }

    public ArrayList<WorkRequestToTrafficControlRoom> getWorkRequestList() {
        return workRequestList;
    }
}
