/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

import java.util.ArrayList;

/**
 *
 * @author DeepaliP
 */
public class DonationDirectory {
    
    private ArrayList<Donor> donorDirectory;
    
    public DonationDirectory(){
        donorDirectory = new ArrayList<>();
    }

    public ArrayList<Donor> getDonorDirectory() {
        return donorDirectory;
    }

    public void setDonorDirectory(ArrayList<Donor> donorDirectory) {
        this.donorDirectory = donorDirectory;
    }
    
//     public Donor addDrug(){
//        Donor donor = new Donor();
//        donorDirectory.add(donor);
//        return donor;
//    }
}
